package kr.ac.mju.mp2019f.testservice;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class BoundService extends Service {
    private final IBinder mBinder = new LocalBinder();
    boolean isRunning = false;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {

        public BoundService getService(){
            return BoundService.this;
        }

    }

    public int addNumbers(int x, int y){
        return x+y;
    }

    public void start() {

            Log.d("MyService", "started" );

    }
    public void stop() {
        Log.d("MyService", "negative" );
    }
}
