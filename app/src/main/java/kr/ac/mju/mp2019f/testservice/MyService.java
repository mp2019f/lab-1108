package kr.ac.mju.mp2019f.testservice;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import javax.xml.datatype.Duration;

public class MyService extends IntentService {

    public MyService() {
        super("MyService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        Log.d("MyService", "Service Started" );
        //Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

        //String msg = intent.getStringExtra("msg");
        String msg = "test";

        int cnt = 0;

        while(cnt < 100) {
            Log.d("MyService", "msg = " + cnt );
            try {
                Thread.sleep(1000);
            } catch( InterruptedException e) {
                e.printStackTrace();
            }
            cnt++;
        }
        stopSelf();

        //return super.onStartCommand(intent, flags, startId);

    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        Log.d("MyService", "Service Stopped" );
//        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
//
//    }
}
