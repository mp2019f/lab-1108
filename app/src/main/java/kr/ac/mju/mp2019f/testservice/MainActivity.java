package kr.ac.mju.mp2019f.testservice;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btStart;
    Button btStop;
    Button btBroadcast;
    Button btNotif;
    BoundService bservice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btStart = findViewById(R.id.btStart);
        btStop = findViewById(R.id.btStop);
        btBroadcast = findViewById(R.id.btBroadcast);
        btNotif = findViewById(R.id.btNotif);

        btStart.setOnClickListener(this);
        btStop.setOnClickListener(this);
        btBroadcast.setOnClickListener(this);
        btNotif.setOnClickListener(this);

        Intent intent = new Intent(this, BoundService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            BoundService.LocalBinder binder = (BoundService.LocalBinder) service;
            bservice = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {};
    };

    @Override
    public void onClick(View view) {
        if( view == btStart ) {
            Log.d("MainActivity", "Start Button Clicked");

            bservice.start();
//            //Intent i = new Intent(this, MyService.class);
//            Intent i = new Intent(this, MyService.class);
//            //i.putExtra("msg", "Hello Service");
//            startService(i);

        } else if( view == btStop)
        {
            Log.d("MainActivity", "Stop Button Clicked");

            bservice.stop();

            // Intent Service는 Stop할 수 없음
            //stopService(new Intent(this, MyService.class) );

        } else if( view == btBroadcast )
        {
            Intent intent = new Intent();
            intent.setAction("MJUAction");
            intent.putExtra("message", "This is my message");
            sendBroadcast(intent);
        } else if( view == btNotif )
        {
            sendNotificationNow();
        }
    }

    public void sendNotificationNow() {

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentIntent(pendingIntent)
                .setSmallIcon(android.R.drawable.stat_notify_voicemail)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle("Hello Sir!!!")
                .setProgress(100, 50, false)
                .setContentText("Something Happened");

        Notification notification = builder.build();

        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification);

        int pos=0;
        while(pos < 100) {
            builder.setProgress(100, pos, false);
            notificationManager.notify(0, builder.build());
            pos++;
            try {
                Thread.sleep(100);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
}
